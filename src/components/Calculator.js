import { React, useState} from "react";
import Container from '@mui/material/Container';
import Box from '@mui/system/Box';
import CalculationHistoric from "./CalculationHistoric";
import './Calculator.css'

export default function Calculator(props) {
    const [num, setNum] = useState(0);
    const [oldnum, setOldNum] = useState(0);
    const [operator, setOperator] = useState();
    
    // Todo usar api conxtext pra poder passar o estados dos calculos pra tabela 

    function inputNum(e) {
        var input = e.target.value;
        num === 0 ? setNum(input) : setNum(num + input);
    }

    function changeSign() {
        num > 0 ? setNum(-num) : setNum(Math.abs(num));
    }

    function porcentage() {
        setNum(num / 100)
    }

    function operatorHandler(e) {
        var operatorInput = e.target.value;
        setOperator(operatorInput);
        setOldNum(num);
        setNum(0);
    }

    function calculate() {
        if (operator === "/") {
          setNum(parseFloat(oldnum) / parseFloat(num));
        } else if (operator === "X") {
          setNum(parseFloat(oldnum) * parseFloat(num));
        } else if (operator === "-") {
            console.log(oldnum)
            console.log(num)
            console.log(oldnum-num)
          setNum(parseFloat(oldnum) - parseFloat(num));
        } else if (operator === "+") {
          setNum(parseFloat(oldnum) + parseFloat(num));
        }
    }

    function clear() {
        setNum(0);
    }

    return (
        <div className="mainCalc">
            <Box m={5} display="flex">
                <Container maxWidth="xs">
                 <h1>Calc Theo</h1>
                    <div className="wraper">
                        <h1 className="result">{num}</h1>
                        <button onClick={clear}>AC</button>
                        <button onClick={changeSign}>+/-</button>
                        <button onClick={porcentage}>%</button>
                        <button className="orange" onClick={operatorHandler} value="/">/</button>
                        <button className="gray" onClick={inputNum} value="7">7</button>
                        <button className="gray" onClick={inputNum} value="8">8</button>
                        <button className="gray" onClick={inputNum} value="9">9</button>
                        <button className="orange" onClick={operatorHandler} value="X">X</button>
                        <button className="gray" onClick={inputNum} value="4">4</button>
                        <button className="gray" onClick={inputNum} value="5">5</button>
                        <button className="gray" onClick={inputNum} value="6">6</button>
                        <button className="orange" onClick={operatorHandler} value="-">-</button>
                        <button className="gray" onClick={inputNum} value="1">1</button>
                        <button className="gray" onClick={inputNum} value="2">2</button>
                        <button className="gray" onClick={inputNum} value="3">3</button>
                        <button className="orange" onClick={operatorHandler} value="+">+</button>
                        <button onClick={inputNum} value="0">0</button>
                        <button onClick={inputNum} value=",">,</button>
                        <button onClick={calculate}>=</button>
                    </div>
                
                </Container>

                <CalculationHistoric>

                </CalculationHistoric>
            </Box>
        </div>
    )
}



